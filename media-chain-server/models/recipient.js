const mongoose = require('mongoose')
const Block = require('../blockFile').Block

const RecipientSchema = mongoose.Schema({
  block: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  viewTimes: {
    type: Object,
    required: true
  },
  original: {
    type: Object,
    required: true
  },
  viewed: {
    type: Object,
    required: true
  }
})

module.exports = mongoose.model('Recipient',RecipientSchema)
