const express = require('express')
const nodemailer = require('nodemailer')


const router = express.Router()

const testChain = require('../blockFile').testChain
const Block = require('../blockFile').Block
const Recipient = require('../models/recipient')

router.post('/', (req,res) => {
  const email = req.body.email
  const password = req.body.password

  Recipient.findOne({'block.hash':password},(err,result) => {
    if(err){
      const auth = {
        authentified: 'false',
        codeExpired: 'false',
      }
      res.send(auth)
      console.log('User unauthorized')
    }
    else{
      if(result && email===result.block.data){
        if(result.viewed==='true'){
          const auth = {
            authentified: 'true',
            codeExpired: 'true',
          }
          res.send(auth)
          console.log('Code Expired')
        }
        else if(result.viewed==='false'){
          const auth = {
            authentified: 'true',
            videoHash: result.block.videoHash,
            codeExpired: 'false',
          }
          res.send(auth)
          console.log('User authorized')

          Recipient.findOne({'block.data':email,original:'true'},(e,r)=>{
            if(e){
              console.log(e)
            }
            else{
              if(r){
                console.log(r)
                var dt = new Date();
                var timestamp = dt.toString();
                var timeStamps = r.viewTimes
                timeStamps.push(timestamp)
                Recipient.updateOne({_id:r._id},{viewTimes:timeStamps},(err,data) => {
                  if(err){
                    console.log(err)
                  }
                  else{
                    console.log(data)
                  }
                })
              }
            }
          })

          Recipient.updateOne({_id:result._id},{viewed:'true'},(err,data) => {
            if(err){
              console.log(err)
            }
            else{
              console.log(data)
            }
          })
        }
      }
      else{
        const auth = {
          authentified: 'false',
          codeExpired: 'false',
        }
        res.send(auth)
        console.log('User unauthorized')
      }
    }
  })
})

module.exports = router;
