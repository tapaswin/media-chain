import React from 'react';
import axios from 'axios'
import '../App.css';
import { Redirect } from 'react-router-dom'
import ReactPlayer from 'react-player'
import { Link } from 'react-router-dom';

class MediaPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authentified: '',
      videoLink: 'https://ipfs.io/ipfs/',
      codeExpired: '',
      noData: 'false'
    }
  }

  componentDidMount(){
    if(this.props.location.aboutProps){
      const recipientData = {
        email: this.props.location.aboutProps.email,
        password: this.props.location.aboutProps.password
      }
      const url='http://localhost:4000/authentify-recipient'
      axios.post(url,recipientData)
        .then(res=> {
          console.log(res.data)
          this.setState((state)=>{
            var videoLink = 'https://ipfs.io/ipfs/'+res.data.videoHash
            console.log(videoLink)
            return ({authentified:res.data.authentified,videoLink:videoLink,codeExpired:res.data.codeExpired})
          })
        })
        .catch(err=>console.log(err.data))
    }
    else{
      this.setState({noData:'true'})
    }
  }

  render() {
    if(this.state.noData==='true'){
      return (
        <div className='videoHomePage'>
          <h1 style={{color:'white'}}>First enter your credentials through login page ....</h1>
          <div style={{height:'80vh'}} className='d-flex flex-column justify-content-end'>
            <Link to='/recipient-login'><button type='submit' style={{fontSize:'30px'}} className='button btn btn-dark m-5'>User's login</button></Link>
          </div>
        </div>
      )
    }
    else if(this.state.authentified==='true' && this.state.codeExpired==='false'){
      return (
        <div id='media-player' className='player-wrapper'>
        <ReactPlayer
          // Disable download button
          config={{ file: { attributes: { controlsList: 'nodownload' } } }}

          // Disable right click
          onContextMenu={e => e.preventDefault()}
          url={this.state.videoLink}
          width='100%'
          height='100%'
          className='react-player'
          progressInterval='10'
          controls={true}
          playing/>
        </div>
      );
    }
    else if(this.state.authentified==='false'){
      console.log('I dont know what HAPPENED')
      return <Redirect to={{pathname:'/recipient-login',aboutProps:{unauthorizedAccess:'true',codeExpired:'false'}}} />
    }
    else if(this.state.authentified==='true' && this.state.codeExpired==='true'){
      console.log('I dont know what HAPPENED')
      return <Redirect to={{pathname:'/recipient-login',aboutProps:{unauthorizedAccess:'false',codeExpired:'true'}}} />
    }
    return (
      <div className='videoHomePage'>
        <h1 style={{color:'white'}}>Verifying your accessibility ....</h1>
      </div>
    )
  }
}

export default MediaPlayer;
