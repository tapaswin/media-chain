const express = require('express')

const router = express.Router()

router.get('/', (req,res) => {
  const data = {
    videoHash: global.videoHash
  }
  res.json(data)
})

module.exports = router;
