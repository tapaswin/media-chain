const SHA256 = require('crypto-js/sha256');
var dt = new Date();
var timestamp = dt.toString();

class Block {
    constructor(index, timestamp, data,name, videoViewd, previousHash = '') {
        this.index = index;
        this.timestamp = timestamp;
        this.data = data;
        this.videoHash = name;
        this.videoViewd = videoViewd;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + JSON.stringify(this.videoHash) + JSON.stringify(this.videoViewd)+ this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
    }

    mineBlock(difficulty) {
      while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){
        this.nonce++;
        this.hash = this.calculateHash();

      }
      console.log("Block mined: " + this.hash);
    }
}

class Blockchain{
    constructor() {
        this.chain = [this.createGenesis()];
        this.difficulty = 4;
    }

    createGenesis() {
        return new Block(0, "14/06/2020", "Genesis block","FIRST USER","none", "0");
    }

    latestBlock() {
        return this.chain[this.chain.length - 1]
    }

    addBlock(newBlock){
        newBlock.previousHash = this.latestBlock().hash;
        newBlock.mineBlock(this.difficulty);
        this.chain.push(newBlock);
    }

    checkValid() {
        for(let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }
            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }
}

let testChain = new Blockchain();

module.exports.testChain = testChain
module.exports.Block = Block


// console.log("Mining block...");
// testChain.addBlock(new Block(1, timestamp,"tapaswin","57d0ca3e051c2d3ce596d6c31941bd6734ad47a95100d8ab08871c9e9016", "video block 1 hash "));
// console.log("Mining block...");

// JSON -getvideo API here.
// convert testchin to JSON to view.

// console.log(testChain)
// console.log(JSON.stringify(testChain, null, 4));
// console.log('Is blockchain valid?' + testChain.checkValid().toString());

// testChain.addBlock(new Block(2, timestamp,"kaale","8bfebb8a68e40da050688ba487dc53820b272b7945f17922058c40a928", "Video  block 2 hash"));
// testChain.addBlock(new Block(3, timestamp,"test-name-3 ","e15e846d8975c81cc44441270442da3e5ec3e449507ab0fdd058c40a92", "Video  block 3 hash"));
