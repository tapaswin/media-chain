const express = require('express')
const Recipient = require('../models/recipient')

const router = express.Router()


router.get('/', async (req,res) => {
  try{
    const data = []
    const recipients = await Recipient.find({original:'true'})
    for(var i=0;i<recipients.length;i++){
      data.push({email:recipients[i].block.data,views:recipients[i].viewTimes.length})
    }
    res.json(data)
  }
  catch(err){
    res.json({message:err})
  }

})

module.exports = router;
