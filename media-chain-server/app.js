const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv/config')

const app = express()

global.videoHash = ''
global.videoName = ''

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods','GET,POST')
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.json())
app.use(express.urlencoded({ extended: false}))

app.use('/add-block',require('./routes/add-block'))
app.use('/get-users',require('./routes/get-users'))
app.use('/send-mails',require('./routes/send-mails'))
app.use('/authentify-recipient',require('./routes/authentify-recipient'))
app.use('/add-video-hash',require('./routes/add-video-hash'))
app.use('/get-video-hash',require('./routes/get-video-hash'))
app.use('/get-video-name',require('./routes/get-video-name'))
app.use('/request-new-password',require('./routes/request-new-password'))


app.get('/',(req,res)=>{
  res.send('Hello there!!! I am server.')
})

mongoose.connect(process.env.DB_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error) => {
    if(error){
      console.log(error)
    }
    else{
      console.log('Connected to DB!!!')
    }
  }
)

const port = process.env.PORT || 4000

app.listen(port,()=>console.log(`Server is up and running!!! at http://localhost:${port}` ))
