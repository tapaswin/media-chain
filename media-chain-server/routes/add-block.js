const express = require('express')
const nodemailer = require('nodemailer')


const router = express.Router()

const testChain = require('../blockFile').testChain
const Block = require('../blockFile').Block
const Recipient = require('../models/recipient')

router.post('/', (req,res) => {
  res.send(req.body);


  // creating recipient block

  var dt = new Date();
  var timestamp = dt.toString();
  var index = testChain.chain.length


  const newBlock = new Block(index, timestamp,req.body.email,global.videoHash, "");
  console.log('****************New block which has been added :');
  console.log(JSON.stringify(newBlock, null, 4));
  console.log(JSON.stringify(testChain, null, 4));
  testChain.addBlock(newBlock);
// Saving data in the database

  const recipient = new Recipient({
    block: newBlock,
    viewTimes: [],
    original: 'true',
    viewed: 'false'
  })

  recipient.save()
  .then(data => {
    console.log(data)
    // Sending email to the recipient

      var auth = {
        user: 'mediachain.1709@gmail.com',
        pass: 'ac_ag_tp@1709'
      }

      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: auth
      })

      const metaData = {
        from: 'mediachain.1709@gmail.com',
        to: req.body.email,
        subject: 'Professional mail(website)!',
        text: `Hello, This is Mediachain. To view video shared to you. \n your login Credential Password : ${data.block.hash} \n Video link : http://localhost:3000/recipient-login`
      }

      transporter.sendMail(metaData,(err,info)=>{
        if(err){
          console.log(err)
        }
        else{
          console.log('The message is sent!')
        }
      })
  })
  .catch(err => {
    console.log(err)
  })


})

module.exports = router;
